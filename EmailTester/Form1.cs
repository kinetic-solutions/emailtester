﻿using System;
using System.Net;
using System.Net.Mail;
using System.Windows.Forms;

namespace EmailTester
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
		}

        private void button1_Click(object sender, EventArgs e)
        {
			if (chkImprovedErrors.Checked)
			{
				try
				{
					Send();
				}
				catch (SmtpException smtpex)
				{
					ShowError(smtpex);
				}
				catch (Exception ex)
				{
					ShowError(ex);
				}
			}
			else
            {
				try
				{
					Send();
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
			}
		}
		private void ShowError(Exception ex)
        {
			if (ex.InnerException == null)
				MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			else
				MessageBox.Show(ex.Message + ". " + ex.InnerException.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			System.IO.File.WriteAllText("stacktrace.txt", ex.StackTrace);
		}
		private void Send()
        {
			if (rbTLS10.Checked)
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
			else if (rbTLS11.Checked)
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
			else if (rbTLS12.Checked)
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
			else if (rbTLS13.Checked)
				ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls13;
			using (MailMessage message = new MailMessage())
			{
				string smtpServer = System.Configuration.ConfigurationManager.AppSettings["SMTPServer"];
				if (string.IsNullOrEmpty(smtpServer))
				{
					throw new SmtpException("SMTPServer is not set");
				}
				Int32.TryParse(System.Configuration.ConfigurationManager.AppSettings["SmtpServerPort"], out int port);
				if (port == 0)
				{
					throw new SmtpException("smtp server port is not set");
				}
				string smtpUser = System.Configuration.ConfigurationManager.AppSettings["SMTPUsername"];
				if (string.IsNullOrEmpty(smtpUser))
				{
					throw new SmtpException("SMTPUsername is not set");
				}
				string smtpPwd = System.Configuration.ConfigurationManager.AppSettings["SMTPPassword"];
				if (string.IsNullOrEmpty(smtpPwd))
				{
					throw new SmtpException("SMTPPassword is not set");
				}
				string smtpFrom = System.Configuration.ConfigurationManager.AppSettings["OverrideFromEmailAddress"];
				if (string.IsNullOrEmpty(smtpFrom))
				{
					throw new SmtpException("OverrideFromEmailAddress is not set");
				}
				if (bool.TryParse(System.Configuration.ConfigurationManager.AppSettings["SMTPSSL"], out bool enableSSL) == false)
					throw new Exception("SMTPSSL not set correctly");
				try
				{
					message.To.Add(new MailAddress(txtToAddress.Text));
				}
				catch (Exception ex2)
				{
					throw new Exception("Error setting to address: " + ex2.Message);
				}
				
				try
				{
					message.From = new MailAddress(smtpFrom);
				}
				catch (Exception ex2)
				{
					throw new Exception("Error setting from address: " + ex2.Message);
				}
				message.Subject = "Test";
				message.Body = txtBody.Text;

				using (SmtpClient s = new SmtpClient())
				{
					s.Host = smtpServer;
					s.Port = port;
					s.Timeout = GetTimeout();
					s.EnableSsl = enableSSL;
					s.Credentials = new NetworkCredential(smtpUser, smtpPwd);
					if (message.Body.ToLower().Contains("<html"))
						message.IsBodyHtml = true;
					try
					{
						s.Send(message);
					}
					catch (Exception ex3)
					{
						throw new Exception("Send error: " + ex3.Message);
					}
				}
			}
			MessageBox.Show("Email sent", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		private int GetTimeout()
		{
			String sTimeout;
			int iTimeout = 30000;

			try
			{
				sTimeout = System.Configuration.ConfigurationManager.AppSettings["email-timeout"];
				if (!Int32.TryParse(sTimeout, out iTimeout))
				{
					iTimeout = 30000;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("GetTimout errored:" + ex.Message);
			}
			return iTimeout;
		}
	}
}
