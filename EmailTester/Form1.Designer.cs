﻿
namespace EmailTester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.txtToAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBody = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.rbTLS11 = new System.Windows.Forms.RadioButton();
            this.rbTLS12 = new System.Windows.Forms.RadioButton();
            this.rbTLS13 = new System.Windows.Forms.RadioButton();
            this.rbTLS10 = new System.Windows.Forms.RadioButton();
            this.chkImprovedErrors = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(375, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 65);
            this.button1.TabIndex = 0;
            this.button1.Text = "Send";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtToAddress
            // 
            this.txtToAddress.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToAddress.Location = new System.Drawing.Point(28, 102);
            this.txtToAddress.Name = "txtToAddress";
            this.txtToAddress.Size = new System.Drawing.Size(328, 28);
            this.txtToAddress.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 22);
            this.label1.TabIndex = 2;
            this.label1.Text = "To Email:";
            // 
            // txtBody
            // 
            this.txtBody.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBody.Location = new System.Drawing.Point(28, 171);
            this.txtBody.Multiline = true;
            this.txtBody.Name = "txtBody";
            this.txtBody.Size = new System.Drawing.Size(486, 275);
            this.txtBody.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 22);
            this.label2.TabIndex = 4;
            this.label2.Text = "Body:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 22);
            this.label3.TabIndex = 5;
            this.label3.Text = "TLS:";
            // 
            // rbTLS11
            // 
            this.rbTLS11.AutoSize = true;
            this.rbTLS11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTLS11.Location = new System.Drawing.Point(165, 34);
            this.rbTLS11.Name = "rbTLS11";
            this.rbTLS11.Size = new System.Drawing.Size(49, 21);
            this.rbTLS11.TabIndex = 6;
            this.rbTLS11.TabStop = true;
            this.rbTLS11.Text = "1.1";
            this.rbTLS11.UseVisualStyleBackColor = true;
            // 
            // rbTLS12
            // 
            this.rbTLS12.AutoSize = true;
            this.rbTLS12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTLS12.Location = new System.Drawing.Point(234, 34);
            this.rbTLS12.Name = "rbTLS12";
            this.rbTLS12.Size = new System.Drawing.Size(49, 21);
            this.rbTLS12.TabIndex = 7;
            this.rbTLS12.TabStop = true;
            this.rbTLS12.Text = "1.2";
            this.rbTLS12.UseVisualStyleBackColor = true;
            // 
            // rbTLS13
            // 
            this.rbTLS13.AutoSize = true;
            this.rbTLS13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTLS13.Location = new System.Drawing.Point(307, 34);
            this.rbTLS13.Name = "rbTLS13";
            this.rbTLS13.Size = new System.Drawing.Size(49, 21);
            this.rbTLS13.TabIndex = 8;
            this.rbTLS13.TabStop = true;
            this.rbTLS13.Text = "1.3";
            this.rbTLS13.UseVisualStyleBackColor = true;
            // 
            // rbTLS10
            // 
            this.rbTLS10.AutoSize = true;
            this.rbTLS10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTLS10.Location = new System.Drawing.Point(93, 34);
            this.rbTLS10.Name = "rbTLS10";
            this.rbTLS10.Size = new System.Drawing.Size(49, 21);
            this.rbTLS10.TabIndex = 9;
            this.rbTLS10.TabStop = true;
            this.rbTLS10.Text = "1.0";
            this.rbTLS10.UseVisualStyleBackColor = true;
            // 
            // chkImprovedErrors
            // 
            this.chkImprovedErrors.AutoSize = true;
            this.chkImprovedErrors.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkImprovedErrors.Location = new System.Drawing.Point(28, 473);
            this.chkImprovedErrors.Name = "chkImprovedErrors";
            this.chkImprovedErrors.Size = new System.Drawing.Size(237, 23);
            this.chkImprovedErrors.TabIndex = 10;
            this.chkImprovedErrors.Text = "Use improved error handling";
            this.chkImprovedErrors.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(543, 506);
            this.Controls.Add(this.chkImprovedErrors);
            this.Controls.Add(this.rbTLS10);
            this.Controls.Add(this.rbTLS13);
            this.Controls.Add(this.rbTLS12);
            this.Controls.Add(this.rbTLS11);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBody);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtToAddress);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Email Test Sender";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtToAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBody;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton rbTLS11;
        private System.Windows.Forms.RadioButton rbTLS12;
        private System.Windows.Forms.RadioButton rbTLS13;
        private System.Windows.Forms.RadioButton rbTLS10;
        private System.Windows.Forms.CheckBox chkImprovedErrors;
    }
}

